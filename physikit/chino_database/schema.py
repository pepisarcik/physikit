class NordicSchema:
    # Chino repository ID
    REPOSITORY_ID = '67ed3520-7da9-499e-87e8-3d29b04f44f5'

    # Chino schema id
    SCHEMA_ID = 'e981fecd-e947-4b5d-9c73-adedb0cdfe55'

    def __init__(self, test_name, result, units, range, test_order_number, test_number, row_number):
        self.row_number = row_number
        self.test_name = test_name
        self.result = result
        self.units = units
        self.range = range
        self.test_order_number = test_order_number
        self.test_number = test_number

    def get_fields(self):

        numbers = ''

        for c in self.result:
            if c.isdigit():
                numbers = numbers + c

        # I know, but it's MVP
        if self.range == '<4 if not supplementing (< 10 nmol/L)':
            optimal_min = 4.0
            optimal_max = 10.0
        elif self.range == '32-100 ng/ml ( 80-250 nmol/L)':
            optimal_min = 32.0
            optimal_max = 100.0
        elif self.range == '32-100':
            optimal_min = 32.0
            optimal_max = 100.0
        else:
            optimal_min = 1.0
            optimal_max = 100.0

        return {
            'row_number': self.row_number,
            'test_name': self.test_name,
            'result': int(numbers),
            'units': self.units,
            'optimal_min': optimal_min,
            'optimal_max': optimal_max,
            'test_order_number': self.test_order_number,
            'test_number': self.test_number
        }
