import os

from chino.api import ChinoAPIClient

class ChinoConnector:

    def import_data(self, data):
        connection = ChinoAPIClient(customer_id=os.getenv('CHINO_CUSTOMER_ID'),
                                    customer_key=os.getenv('CHINO_API_KEY'),
                                    url=os.getenv('CHINO_API_URL'))

        for schema in data:
            query = {
                "and": [
                    {
                        "field": "test_order_number",
                        "type": "eq",
                        "value": schema.test_order_number,
                    },
                    {
                        "field": "test_number",
                        "type": "eq",
                        "value": schema.test_number,
                    },
                    {
                        "field": "row_number",
                        "type": "eq",
                        "value": schema.row_number,
                    },
                ]
            }

            exists = connection.documents.search(schema.SCHEMA_ID, query)

            if not exists:
                connection.documents.create(schema.SCHEMA_ID, schema.get_fields())
            else:
                continue

