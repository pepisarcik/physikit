import os
from zeep import Client
import hashlib
import json
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import StringIO
import numpy as np


class NordicLab:
    TEST_FILE_NAME = 'test_result.pdf'
    INVALID_KEY = 'TestNo'

    TEST_NAME_START_KEY = 'Test Name'
    TEST_NAME_END_KEY = 'Therapies'

    RESULT_START_KEY = 'Result'
    RESULT_END_KEY = 'Units'

    UNITS_START_KEY = 'Units'
    UNITS_END_KEY = 'Range'

    RANGE_START_KEY = 'Range'
    RANGE_END_KEY = 'L'

    def get_test_result(self, test_order_number, test_number):
        pdf_url = self.get_pdf_url(test_order_number, test_number)
        pdf_text = self.get_text_from_pdf(pdf_url)
        pdf_data = self.parse_pdf_text(pdf_text)

        return self.transform_data_list(pdf_data)

    def transform_data_list(self, data):
        transposed_data = np.transpose(data).tolist()

        transposed_data.pop(0)

        return transposed_data

    def get_text_from_pdf(self, file):
        rsrcmgr = PDFResourceManager()
        retstr = StringIO()
        laparams = LAParams()
        device = TextConverter(rsrcmgr, retstr, laparams=laparams)

        fp = open(self.TEST_FILE_NAME, 'rb')

        interpreter = PDFPageInterpreter(rsrcmgr, device)

        for page in PDFPage.get_pages(fp, set(), maxpages=0, caching=True, check_extractable=True):
            interpreter.process_page(page)

        text = retstr.getvalue()

        fp.close()
        device.close()
        retstr.close()

        return text

    def get_text_keys(self):
        return [
            ('test_name', self.TEST_NAME_START_KEY, self.TEST_NAME_END_KEY),
            ('result', self.RESULT_START_KEY, self.RESULT_END_KEY),
            ('units', self.UNITS_START_KEY, self.UNITS_END_KEY),
            ('range', self.RANGE_START_KEY, self.RANGE_END_KEY),
        ]

    def parse_pdf_text(self, text):
        parsed_data = []

        for index, start_key, end_key in self.get_text_keys():
            parsed_data.append(self.get_data_from_text(text.splitlines(), index, start_key, end_key))

        return parsed_data

    def get_data_from_text(self, lines, index_name, start_key, end_key):
        start_index, end_index = self.get_start_end_indexes(lines, start_key, end_key)

        test_names = [index_name]

        index = 0

        while index < end_index:
            if lines[index] and index > start_index:
                test_names.append(lines[index])
            index += 1

        return test_names

    def get_start_end_indexes(self, lines, start_key, end_key):
        index = 0
        start_index = 0
        end_index = 0

        for line in lines:
            if line == start_key:
                start_index = index
            elif line == end_key:
                end_index = index
                break
            index += 1

        return start_index, end_index

    def get_pdf_url(self, test_order_number, test_number):
        client = Client(os.getenv('NORDIC_LAB_URL'))

        username = os.getenv('NORDIC_LAB_ACCESS_USER_NAME')
        password = os.getenv('NORDIC_LAB_USER_PASSWORD')
        account_number = os.getenv('NORDIC_LAB_ACCOUNT_NUMBER')
        test_order_number = test_order_number
        test_number = test_number
        hash_key = os.getenv('NORDIC_LAB_HASH_KEY')

        hash_string = username + password + account_number + test_order_number + test_number + hash_key

        hash = hashlib.md5(hash_string.encode('utf-8')).hexdigest().upper()

        result = client.service.GetResultPDFLink(username, password, account_number, test_order_number, test_number,
                                                 hash)

        fixed_json_string = self.fix_invalid_json_string(result)

        data_json = json.loads(fixed_json_string)

        return data_json.get('testresults')[0]['TestResult']

    def fix_invalid_json_string(self, json):
        fixed_json = ''

        for line in json.splitlines():
            key = line.split(':')[0].replace("\"", "").strip()
            if key == self.INVALID_KEY:
                fixed_json += line + ","
            else:
                fixed_json += line

        return fixed_json
