
from physikit.chino_database.schema import NordicSchema
from physikit.chino_database.connector import ChinoConnector
from physikit.nordic_lab.connector import NordicLab

from dotenv import load_dotenv
import argparse

def main(args):
    # Testing data
    # test_order_number = 'TST-NL-84683'
    # test_number = 'T-NL-1039452'


    test_order_number = args.test_order_number
    test_number = args.test_number

    nordic_lab = NordicLab()
    test_results = nordic_lab.get_test_result(test_order_number, test_number)

    chino = ChinoConnector()

    nordic_lab_schemas = []

    row_number = 1

    for result in test_results:
        nordic_lab_schemas.append(NordicSchema(result[0], result[1], result[2], result[3], test_order_number, test_number, row_number))
        row_number += 1


    chino.import_data(nordic_lab_schemas)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process laboratory tests')
    parser.add_argument('--test_order_number', metavar='ton', type=str, help='Test order number')
    parser.add_argument('--test_number', metavar='tn', type=str, help='Test number')

    args = parser.parse_args()

    load_dotenv()
    main(args)
